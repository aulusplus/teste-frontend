# TESTE FRONT-END
 
## GUIA
- Clonar o repositório teste-front (https://bitbucket.org/aulusplus/teste-front);
- Criar um branch (com seu nome) com base do master;
- Fatiar o teste-jr.psd no branch criado (psd está na pasta _arquivos); 

### USO OPCIONAL:
- CSS Grid;
- Sass/Less;
- Grunt/Gulp;